package com.zmg.mobileTest;

import com.zmg.mobileTest.api.ApiService;
import com.zmg.mobileTest.api.ApiServiceClient;
import com.zmg.mobileTest.model.Comment;
import com.zmg.mobileTest.model.Post;

import org.junit.Test;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

import static org.junit.jupiter.api.Assertions.*;



public class PlaceholderUnitTest {

    ApiService service;

    @Test
    public void getPosts() {
        synchronized (this) {
            service = ApiServiceClient.initializeService();
            Call<List<Post>> call = service.getPosts();
            try {
                Response<List<Post>> response = call.execute();
                assertEquals(200, response.code());
                List<Post> posts = response.body();
                assertEquals(100, posts.size());
            } catch (Exception e) {
                e.printStackTrace();
                fail(e.getMessage());
            }
        }
    }

    @Test
    public void getComments() {
        synchronized (this) {
            service = ApiServiceClient.initializeService();
            HashMap<String, String> queryMap = new HashMap<>();
            queryMap.put("postId", "1");
            Call<List<Comment>> call = service.getComments(queryMap);
            try {
                Response<List<Comment>> response = call.execute();
                assertEquals(200, response.code());
                List<Comment> comments = response.body();
                assertEquals(5, comments.size());
            } catch (Exception e) {
                e.printStackTrace();
                fail(e.getMessage());
            }
        }
    }
}