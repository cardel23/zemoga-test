package com.zmg.mobileTest;

import com.orm.SugarApp;

public class PlaceholderApp extends SugarApp {

    private static PlaceholderApp instance;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public static PlaceholderApp getInstance(){
        if(instance == null){
            instance = (PlaceholderApp)new PlaceholderApp().getSugarContext();
        }
        return instance;
    }
}
