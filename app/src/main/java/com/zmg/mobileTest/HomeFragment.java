package com.zmg.mobileTest;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.zmg.mobileTest.ui.PostAdapter;
import com.zmg.mobileTest.databinding.FragmentHomeBinding;
import com.zmg.mobileTest.api.OnPostExecuteListener;
import com.zmg.mobileTest.model.Post;
import com.zmg.mobileTest.api.PostTask;

import java.util.List;

public class HomeFragment extends Fragment implements View.OnClickListener, OnPostExecuteListener<List<Post>> {

    private FragmentHomeBinding binding;
    private RecyclerView recyclerView;
    private PostAdapter postAdapter;
    private PostTask postTask;
    FloatingActionButton fab_add, fab_clr;
    MainActivity activity;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        recyclerView = binding.recycler;
        activity = (MainActivity) getActivity();
        postAdapter = activity.getPostAdapter();
        postAdapter.setFam(binding.fam);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(postAdapter);
        recyclerView.getRecycledViewPool().setMaxRecycledViews(0, 0);

        fab_add = binding.fabAdd;
        fab_clr = binding.fabClr;

        fab_add.setOnClickListener(this);
        fab_clr.setOnClickListener(this);

        return view;

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (postAdapter.getItems().isEmpty())
            createAndExecuteTask(this);

    }

    void createAndExecuteTask(OnPostExecuteListener<List<Post>> onPostExecuteListener) {
        postTask = new PostTask(getContext());
        postTask.setOnPostExecuteListener(onPostExecuteListener);
        postTask.execute();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.fab_clr)
            postAdapter.removeAll();
        if(view.getId() == R.id.fab_add)
            createAndExecuteTask(this);
        binding.fam.collapse();
    }

    @Override
    public void onPostExecute(List<Post> response, List<String> errorMessages) {
        if(response != null | !response.isEmpty()) {
            postAdapter.getItems().addAll(response);
            postAdapter.notifyDataSetChanged();
        }
    }
}