package com.zmg.mobileTest.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.zmg.mobileTest.R;
import com.zmg.mobileTest.ScrollingActivity;
import com.zmg.mobileTest.databinding.PostViewBinding;
import com.zmg.mobileTest.model.Post;

import java.util.List;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.ViewHolder> {

    private List<Post> mValues;
    private final Context mContext;
    private SparseBooleanArray mSelectedItemsIds;
    boolean isActionMode = false;
    ActionMode mode = null;
    int nr = 0;
    FloatingActionsMenu fam;


    public PostAdapter(Context context) {
        mContext = context;
        mSelectedItemsIds = new SparseBooleanArray();
        loadPosts();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = PostViewBinding.inflate(LayoutInflater.from(mContext), parent, false).getRoot();
        return new ViewHolder(v);

    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Post post = getItems().get(position);
        holder.title.setText(post.getTitle());
        holder.itemView.setBackgroundColor(mSelectedItemsIds.get(position) ? 0x9934B5E4
                : Color.TRANSPARENT);
        if (!post.isFav()) holder.imageView.setVisibility(View.GONE);
        holder.itemView.setOnClickListener(view -> {
            fam.collapse();
            if (isActionMode) {
                clickItem(holder);
            } else {
                int position1 = holder.getBindingAdapterPosition();
                Post post1 = mValues.get(position1);
                Bundle extras = new Bundle();
                extras.putInt("postId", post1.getPostId());
                Intent intent = new Intent(mContext, ScrollingActivity.class);
                intent.putExtras(extras);
                mContext.startActivity(intent);
            }
        });
        holder.itemView.setOnLongClickListener(view -> {
            if (!isActionMode) {
                view.setSelected(true);
                ActionMode.Callback2 callback = new ActionMode.Callback2() {


                    @Override
                    public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
                        nr = 0;
                        mode = actionMode;
                        MenuInflater inflater = actionMode.getMenuInflater();
                        inflater.inflate(R.menu.menu_scrolling, menu);
                        fam.setVisibility(View.GONE);
                        return true;
                    }

                    @Override
                    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
                        isActionMode = true;
                        clickItem(holder);

                        return true;
                    }

                    @Override
                    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.menu_delete:
                                SparseBooleanArray selected = getSelectedIds();
                                for (int i = selected.size() - 1; i >= 0; i--) {
                                    if (selected.valueAt(i)) {
                                        remove(selected.keyAt(i));
                                        notifyDataSetChanged();
                                    }
                                }
                                actionMode.finish();
                                return true;
                            case R.id.menu_select_all:
                                if (getSelectedIds().size() < getItemCount()) {
                                    removeSelection();
                                    nr = 0;
                                    for (int i = 0; i < getItemCount(); i++) {
                                        selectItem(i);
                                    }
                                }
                                return true;
                            default:
                                return false;
                        }
                    }

                    @Override
                    public void onDestroyActionMode(ActionMode actionMode) {
                        isActionMode = false;
                        view.setSelected(false);
                        removeSelection();
                        fam.setVisibility(View.VISIBLE);
                        mode = null;
                    }
                };
                ((AppCompatActivity) view.getContext()).startActionMode(callback);
            }
            return true;
        });

    }

    private void clickItem(ViewHolder holder) {
        int position = holder.getBindingAdapterPosition();
        selectItem(position);
    }

    private void selectItem(int position) {
        if(!getSelectedIds().get(position))
            nr ++;
        else nr --;
        toggleSelection(position);

        if(nr == 0 && isActionMode) {
            mode.finish();
            isActionMode = false;
        }
        if (mode != null)
            mode.setTitle(mContext.getString(R.string.selected, nr));
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void setFam(FloatingActionsMenu fam) {
        this.fam = fam;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public ImageView imageView;

        public ViewHolder(View v) {
            super(v);
            title = v.findViewById(R.id.title);
            imageView = v.findViewById(R.id.star);

            v.setLongClickable(true);


        }

        @NonNull
        @Override
        public String toString() {
            return title.getText().toString();
        }
    }

    public List<Post> getItems() {
        return mValues;
    }

    public void toggleSelection(int position){
        selectView(position, !mSelectedItemsIds.get(position));
    }

    public void removeSelection() {
        mSelectedItemsIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }

    public void selectView(int position, boolean value) {
        if (value)
            mSelectedItemsIds.put(position, true);
        else
            mSelectedItemsIds.delete(position);

        notifyDataSetChanged();
    }

    public void remove(int position){
        if(mValues != null && !mValues.isEmpty()){
            mValues.get(position).delete();
            mValues.remove(position);
            notifyDataSetChanged();
        }
    }

    public void removeAll() {
        Post.deleteAll(Post.class);
        mValues.removeAll(getItems());
        notifyDataSetChanged();
    }

    public void loadPosts() {
        mValues = Post.findWithQuery(Post.class, "select * from post order by fav desc, id asc", (String[]) null);
    }

    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }


}