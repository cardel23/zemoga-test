package com.zmg.mobileTest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.util.List;

public class Post extends SugarRecord<Post> {

   @Expose
   @SerializedName("userId")
   private int userId;
   @Expose
   @SerializedName("id")
   private int postId;
   @Expose
   @SerializedName("title")
   private String title;
   @Expose
   @SerializedName("body")
   private String body;
   private boolean fav;

   private List<Comment> comments;

   public int getUserId() {
      return userId;
   }

   public void setUserId(int userId) {
      this.userId = userId;
   }

   public int getPostId() {
      return postId;
   }

   public void setPostId(int postId) {
      this.postId = postId;
   }

   public String getTitle() {
      return title;
   }

   public void setTitle(String title) {
      this.title = title;
   }

   public String getBody() {
      return body;
   }

   public void setBody(String body) {
      this.body = body;
   }

   public boolean isFav() {
      return fav;
   }

   public void setFav(boolean fav) {
      this.fav = fav;
   }

   public List<Comment> getComments() {
      if (comments == null)
         comments = Comment.findWithQuery(Comment.class, "select * from comment where post_id=?", String.valueOf(postId));
      return comments;
   }

   public void setComments(List<Comment> comments) {
      this.comments = comments;
   }
}
