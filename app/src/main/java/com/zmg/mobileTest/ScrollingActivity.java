package com.zmg.mobileTest;

import android.graphics.drawable.Drawable;
import android.os.Bundle;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.zmg.mobileTest.ui.CommentAdapter;
import com.zmg.mobileTest.databinding.ActivityScrollingBinding;
import com.zmg.mobileTest.api.OnPostExecuteListener;
import com.zmg.mobileTest.model.Post;
import com.zmg.mobileTest.api.PostDetailTask;

import java.util.List;

public class ScrollingActivity extends AppCompatActivity {

    private ActivityScrollingBinding binding;
    Post post;
    CommentAdapter commentAdapter;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityScrollingBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        Toolbar toolbar = binding.toolbar;
        setSupportActionBar(toolbar);
        CollapsingToolbarLayout toolBarLayout = binding.toolbarLayout;
        toolBarLayout.setTitle("Loading...");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NestedScrollView scrollView = binding.included.scrollView;
        TextView body = scrollView.findViewById(R.id.bodyTextView);
        recyclerView = scrollView.findViewById(R.id.comments_view);
        FloatingActionButton fab = binding.fab;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                post.setFav(!post.isFav());
                post.save();
                setButtonIcon();
                String message = post.isFav() ? "added to" : "removed from";
                Toast.makeText(ScrollingActivity.this, "The post has been "+message+ " your favorites", Toast.LENGTH_SHORT).show();
            }
        });

        int postId = getIntent().getIntExtra("postId", 0);

        PostDetailTask detailTask = new PostDetailTask(this);
        detailTask.setOnPostExecuteListener(new OnPostExecuteListener<Post>() {
            @Override
            public void onPostExecute(Post response, List<String> errorMessages) {
                if (response != null) {
                    post = response;
                    toolBarLayout.setTitle(response.getTitle());
                    body.setText(response.getBody());
                    commentAdapter = new CommentAdapter(ScrollingActivity.this, response.getComments());

                    LinearLayoutManager layoutManager = new LinearLayoutManager(ScrollingActivity.this);
                    DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                            DividerItemDecoration.VERTICAL);
                    recyclerView.addItemDecoration(dividerItemDecoration);
                    recyclerView.setLayoutManager(layoutManager);
                    recyclerView.setAdapter(commentAdapter);
                    setButtonIcon();

                } else {
                    Toast.makeText(ScrollingActivity.this, "Error while loading comments", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });
        detailTask.execute(postId);
    }

    void setButtonIcon() {
        Drawable d = post.isFav() ? getDrawable(R.drawable.ic_star_24) : getDrawable(R.drawable.ic_star_outline_24);
        binding.fab.setImageDrawable(d);
    }
}