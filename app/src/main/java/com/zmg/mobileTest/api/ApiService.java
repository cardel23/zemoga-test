package com.zmg.mobileTest.api;

import com.zmg.mobileTest.model.Comment;
import com.zmg.mobileTest.model.Post;
import com.zmg.mobileTest.util.Utils;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface ApiService {

    @GET(Utils.POSTS)
    Call<List<Post>> getPosts();

    @GET(Utils.COMMENTS)
    Call<List<Comment>> getComments(@QueryMap Map<String, String> query);
}
