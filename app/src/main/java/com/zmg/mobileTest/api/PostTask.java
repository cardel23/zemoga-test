package com.zmg.mobileTest.api;

import android.content.Context;

import com.zmg.mobileTest.model.Post;

import java.util.List;

import retrofit2.Response;

public class PostTask extends TaskWithCallback<Void, Void, List<Post>> {

    public PostTask(Context context) {
        super(context);
    }

    @Override
    protected List<Post> doInBackground(Void... voids) {

        try {
            Response<List<Post>> response = service.getPosts().execute();
            responseElement = response.body();
            for (Post p :
                    responseElement) {
                p.save();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseElement;
    }
}
