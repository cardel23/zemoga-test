package com.zmg.mobileTest.api;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.widget.Toast;

import com.zmg.mobileTest.PlaceholderApp;
import com.zmg.mobileTest.R;

import java.util.ArrayList;
import java.util.HashMap;

public abstract class TaskWithCallback<T1, T2, T3> extends AsyncTask<T1, T2, T3> {
    protected PlaceholderApp app;
    protected ApiService service;
    protected Context context;
    protected ArrayList<String> errorMessages;
    protected OnPostExecuteListener<T3> onPostExecuteListener;
    protected ProgressDialog progressDialog;
    protected String loadingMessage;
    protected Integer timeout;
    protected Integer timer = 0;
    protected HashMap<String, String> queryParams = new HashMap<String, String>();
    protected StringBuilder query = new StringBuilder();
    protected T3 responseElement;

    public TaskWithCallback(Context context) {
        this.context = context;
        this.app = (PlaceholderApp) context.getApplicationContext();
        responseElement = null;
    }

    public TaskWithCallback(Context context, String loadingMessage) {
        this.context = context;
        this.app = (PlaceholderApp) context.getApplicationContext();
        this.loadingMessage = loadingMessage;
    }

    public PlaceholderApp getApp() {
        return app;
    }

    public void setApp(PlaceholderApp app) {
        this.app = app;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public OnPostExecuteListener<T3> getOnPostExecuteListener() {
        return onPostExecuteListener;
    }

    public void setOnPostExecuteListener(
            OnPostExecuteListener<T3> onPostExecuteListener) {
        this.onPostExecuteListener = onPostExecuteListener;
    }

    public ArrayList<String> getErrorMessages() {
        return errorMessages;
    }

    public void setErrorMessages(ArrayList<String> errorMessages) {
        this.errorMessages = errorMessages;
    }

    public Integer getTimeout() {
        if (timeout == null)
            timeout = 10000;
        return timeout;
    }

    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (service == null)
            service = ApiServiceClient.initializeService();
        // app.setLocaleConfig();
        String message;
        if (loadingMessage != null)
            message = loadingMessage;
        else
            message = app.getString(R.string.loading);
        progressDialog = ProgressDialog.show(this.context, "", message, true, true, new DialogInterface.OnCancelListener() {

            public void onCancel(DialogInterface dialog) {
                // TODO Auto-generated method stub
                progressDialog.cancel();
                TaskWithCallback.this.cancel(true);
            }
        });
    }

    @Override
    protected void onPostExecute(T3 response) {
        super.onPostExecute(response);

//		progressDialog.dismiss();
        if (this.onPostExecuteListener != null) {
            this.onPostExecuteListener.onPostExecute(response, errorMessages);
        }
        try {
            progressDialog.dismiss();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            Toast.makeText(app, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onCancelled() {
        // TODO Auto-generated method stub
        super.onCancelled();
        this.cancel(true);
    }

    @Override
    protected void onProgressUpdate(T2... values) {
        // TODO Auto-generated method stub
        super.onProgressUpdate(values);
        if (timer.equals(timeout))
            this.cancel(true);
    }

    protected T3 executeSynchronous(T1... params) {
        return null;
    }
}