package com.zmg.mobileTest.api;

import android.content.Context;

import com.zmg.mobileTest.model.Comment;
import com.zmg.mobileTest.model.Post;

import java.io.IOException;
import java.util.List;

import retrofit2.Response;

public class PostDetailTask extends TaskWithCallback<Integer, Void, Post>{

    public PostDetailTask(Context context) {
        super(context);
    }

    @Override
    protected Post doInBackground(Integer... integers) {
        int id = integers[0];
        Post post;
        List<Comment> comments;
        try {
            post = Post.findWithQuery(Post.class, "select * from POST where post_id = ?", String.valueOf(id)).get(0);
            if (post.getComments().isEmpty()) {
                queryParams.put("postId", String.valueOf(id));
                Response<List<Comment>> response = service.getComments(queryParams).execute();
                comments = response.body();
                if (!comments.isEmpty()) {
                    for (Comment c :
                            comments) {
                        c.save();
                    }
                }
                post.setComments(comments);
            }
        } catch (NullPointerException | IOException e) {
            errorMessages.add(e.getMessage());
            return null;
        }
        return post;
    }
}
