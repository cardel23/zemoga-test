package com.zmg.mobileTest.api;

import android.util.Log;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.zmg.mobileTest.util.Utils;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiServiceClient {

    private static ApiService apiService;

    public static ApiService initializeService() {
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .readTimeout(40, TimeUnit.SECONDS)
                .connectTimeout(40, TimeUnit.SECONDS)
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {

                        Request request = chain.request();
                        Request newRequest = request.newBuilder()
                                .header("Accept", "application/json")
                                .build();
                        Response originalResponse = chain.proceed(newRequest);
                        ResponseBody body = originalResponse.body();
                        String bodyString = body.string();
                        String requestString = originalResponse.request().url().url().toString();
                        Log.i("APIServiceClient","URL: " + requestString);
                        MediaType contentType = body.contentType();
                        Response newResponse = originalResponse.newBuilder().body(ResponseBody.create(contentType, bodyString)).build();
                        Log.i("APIServiceClient", "Response: " + bodyString);
                        return newResponse;
                    }
                })
                .build();
        Gson gson = new GsonBuilder().setFieldNamingPolicy(
                FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .excludeFieldsWithoutExposeAnnotation()
                .create();

        ;

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Utils.SERVER)
                .client(httpClient)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson));

        Retrofit adapter = builder.build();
        apiService = adapter.create(ApiService.class);
        return apiService;
    }
}
