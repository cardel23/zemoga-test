package com.zmg.mobileTest.api;

import java.util.List;


public interface OnPostExecuteListener<T> {
    public void onPostExecute(T response, List<String> errorMessages);
}
