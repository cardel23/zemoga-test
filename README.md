# Welcome

This is the app made for the ZEMOGA Mobile Test

## Project Description
The basic package structure is as follows:
```
- com.zmg.mobileTest
	- api
	- model
	- ui
```

`com.zmg.mobileTest`: this is the root package where all the activities and related classes are located.
`com.zmg.mobileTest.api`: has all the classes that handle REST operations.
`com.zmg.mobileTest.model`: contains two Java classes serving as entities: 
- Post
- Comment

These classes are a POJO serialized version of the response entities from [JSONPlaceholder](https://jsonplaceholder.typicode.com/) resources `/posts` and `/comments` and perform persistency operations.

`com.zmg.mobileTest.ui`: contains view adapters to help preview the data.
`com.zmg.mobileTest.util`: for static resources that can be changed for different android flavors or compile versions.

The test folders contain a simple JUnit test class `PlaceholderUnitTest` located in the root package `com.zmg.mobileTest`.

## Dependencies

This app relies on [SugarORM](https://satyan.github.io/sugar/) for the persistency layer management. There much more powerful alternatives such as Realm, but for the scope of this SugarORM was the choice because of its lightweight and simplicity to implement.

For HTTP connection, the solution is [Retrofit](https://square.github.io/retrofit/), a well known http client for android.

[Zendesk's](https://github.com/zendesk/android-floating-action-button) implementation of the FloatingActionButton for drawing [Material Design promoted actions](http://www.google.com/design/spec/patterns/promoted-actions.html).

## Installation
This project requires at least Java 8 for compilation and Android API 24.

In order to build and deploy the resources is necessary having AndroidStudio with Gradle installed.

To run the project simply clone it into AndroidStudio and run the project in any virtual or physical device that supports the respective Android API (_Android_ 7.0) version.

To clone the project copy this into AndroidStudio's `Get from Version Control` window or to a terminal and then import the project manually:
``` 
git clone git@bitbucket.org:cardel23/zemoga-test.git
```
In case there is any trouble with compilation, there is a signed apk in the `app/release` folder.

